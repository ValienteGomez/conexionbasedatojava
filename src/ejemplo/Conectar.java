
package ejemplo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class Conectar {
    private Connection con;

    public Conectar() {
        obtenerConecxion();
    }
    private void obtenerConecxion(){
        try{
            con=(Connection)DriverManager.getConnection("jdbc:mysql://localhost/ejemplo", "root", "");
            //JOptionPane.showMessageDialog(null, "Conexion establecida");
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error de conexion");
            System.exit(0);
        }
    }
    public Statement crearSentencia(){
       try{
        return con.createStatement();
       }
       catch(SQLException e){
           return null;
       }
}
}
